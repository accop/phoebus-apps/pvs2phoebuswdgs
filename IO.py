## Developer: Benjamin Bolling, 2022-08-30.
## Based on script by Georg Weiss, 2022-02-23.

import xml.etree.ElementTree as ET

def getXMLcontents(fname):
    xml = ET.parse(fname).getroot()
    devices = []
    for device in xml.iter("device"):
        macros = dict()
        for el in device:
            macros[el.tag] = el.text
        devices.append(macros)
    return devices

def getDictFileContents(fname):
    devices = []
    with open(fname, "r") as f:
        for device in f.read().split('\n'):
            line = device.split(',')
            if len(line) == 2:
                macros = dict()
                macros['NAME'] = line[0]
                macros['PV'] = line[1]
                devices.append(macros)
    return devices

def buildFile(wdg,devices,dest,wdt,hgt,rows):
    f = open(dest, "w")
    lbl_wdt = 90
    createHeader(f,wdg,len(devices),wdt,hgt,rows,lbl_wdt)
    xpos, ypos, row, col = 0, 0, 0, 0
    for i in range(len(devices)):
        createLabel(f,devices[i]['NAME'],xpos,ypos,lbl_wdt,hgt)
        createWidget(f,wdg,devices[i],xpos,ypos,wdt,hgt,lbl_wdt)
        if row == rows:
            ypos, row = 0, 0
            col += 1
            xpos = col * (lbl_wdt + wdt + 20)
        else:
            row += 1
            ypos = row * (hgt + 10)
    createFooter(f)
    f.close()

def createHeader(f,wdg,n,wdt,hgt,rows,lbl_wdt):
    if n > rows:
        win_hgt = rows * (hgt + 10)
        cols = int(n / rows) + (n % rows > 0)
        win_wdt = cols * (lbl_wdt + wdt + 20)
    else:
        win_hgt = n * (hgt + 10)
        win_wdt = lbl_wdt + 20 + wdt
    f.write('<?xml version="1.0" encoding="UTF-8"?>\n')
    f.write('<display version="2.0.0">\n')
    f.write('  <name>Display of list of {} widgets</name>\n'.format(wdg))
    f.write('  <width>{}</width>\n'.format(win_wdt))
    f.write('  <height>{}</height>\n'.format(win_hgt))

def createLabel(f,name,xpos,ypos,wdt,hgt):
    f.write('  <widget type="label" version="2.0.0">\n')
    f.write('    <name>{}_lbl</name>\n'.format(name))
    f.write('    <text>{}:</text>\n'.format(name))
    f.write('    <x>{}</x>\n'.format(xpos))
    f.write('    <y>{}</y>\n'.format(ypos))
    f.write('    <width>{}</width>\n'.format(wdt))
    f.write('    <height>{}</height>\n'.format(hgt))
    f.write('    <horizontal_alignment>2</horizontal_alignment>\n')
    f.write('    <vertical_alignment>1</vertical_alignment>\n')
    f.write('  </widget>\n')

def createWidget(f,wdg,device,xpos,ypos,wdt,hgt,lbl_wdt):
    if wdg == 'Button':
        createBtn(f,device,xpos,ypos,wdt,hgt,lbl_wdt)
    elif wdg == 'LED':
        createLED(f,device,xpos,ypos,wdt,hgt,lbl_wdt)
    elif wdg == 'Text Update':
        createTextUpd(f,device,xpos,ypos,wdt,hgt,lbl_wdt)
    elif wdg == 'Check box':
        createCheckBox(f,device,xpos,ypos,wdt,hgt,lbl_wdt)
    elif wdg == 'Spinner':
        createSpinner(f,device,xpos,ypos,wdt,hgt,lbl_wdt)
    elif wdg == 'Combo Box':
        createComboBox(f,device,xpos,ypos,wdt,hgt,lbl_wdt)
def createFooter(f):
    f.write('</display>\n')

def writePosDim(f,xpos,ypos,wdt,hgt,lbl_wdt):
    f.write('    <x>{}</x>\n'.format(xpos+10+lbl_wdt))
    f.write('    <y>{}</y>\n'.format(ypos))
    f.write('    <width>{}</width>\n'.format(wdt))
    f.write('    <height>{}</height>\n'.format(hgt))

def writeGenericMonitoringWdg(f,device,xpos,ypos,wdt,hgt,lbl_wdt):
    f.write('    <name>{}</name>\n'.format(device['NAME']))
    f.write('    <pv_name>{}</pv_name>\n'.format(device['PV']))
    writePosDim(f,xpos,ypos,wdt,hgt,lbl_wdt)
    f.write('    <tooltip>$(pv_name)\n$(pv_value)</tooltip>\n')
    f.write('  </widget>\n')

def createBtn(f,device,xpos,ypos,wdt,hgt,lbl_wdt):
    f.write('  <widget type="action_button" version="3.0.0">\n')
    f.write('    <name>{}</name>\n'.format(device['NAME']))
    f.write('    <pv_name>{}</pv_name>\n'.format(device['PV']))
    writePosDim(f,xpos,ypos,wdt,hgt,lbl_wdt)
    f.write('    <tooltip>$(actions)</tooltip>\n')
    f.write('  </widget>\n')

def createLED(f,device,xpos,ypos,wdt,hgt,lbl_wdt):
    f.write('  <widget type="led" version="2.0.0">\n')
    writeGenericMonitoringWdg(f,device,xpos,ypos,wdt,hgt,lbl_wdt)

def createTextUpd(f,device,xpos,ypos,wdt,hgt,lbl_wdt):
    f.write('  <widget type="textupdate" version="2.0.0">\n')
    writeGenericMonitoringWdg(f,device,xpos,ypos,wdt,hgt,lbl_wdt)

def createCheckBox(f,device,xpos,ypos,wdt,hgt,lbl_wdt):
    f.write('  <widget type="checkbox" version="2.0.0">\n')
    writeGenericMonitoringWdg(f,device,xpos,ypos,wdt,hgt,lbl_wdt)

def createSpinner(f,device,xpos,ypos,wdt,hgt,lbl_wdt):
    f.write('  <widget type="spinner" version="2.0.0">\n')
    writeGenericMonitoringWdg(f,device,xpos,ypos,wdt,hgt,lbl_wdt)

def createComboBox(f,device,xpos,ypos,wdt,hgt,lbl_wdt):
    f.write('  <widget type="combo" version="2.0.0">\n')
    writeGenericMonitoringWdg(f,device,xpos,ypos,wdt,hgt,lbl_wdt)
