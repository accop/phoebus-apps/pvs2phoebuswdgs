## Developer: Benjamin Bolling, 2022-08-30.
## Based on script by Georg Weiss, 2022-02-23.

import IO
import tkinter as tk
from tkinter import filedialog, simpledialog

class simpleChoice(simpledialog.Dialog):
    def __init__(self, parent, title, text, items):
        self.selection, self._items, self._text = None, items, text
        super().__init__(parent, title=title)

    def body(self, parent):
        self._message = tk.Message(parent, text=self._text, aspect=300)
        self._message.pack(expand=1, fill=tk.BOTH)
        self._list = tk.Listbox(parent)
        self._list.pack(expand=1, fill=tk.BOTH, side=tk.TOP)
        for item in self._items:
            self._list.insert(tk.END, item)
        return self._list

    def validate(self):
        if not self._list.curselection():
            return 0
        return 1

    def apply(self):
        self.selection = self._items[self._list.curselection()[0]]

if __name__ == '__main__':
    win = tk.Tk()
    win.withdraw()

    wdg = simpleChoice(win, 'Widget Selection', text='Select widget specie for the PVs:',
                       items=['Button', 'LED', 'Text Update', 'Check box', 'Spinner', 'Combo Box']).selection

    tk.Tk().withdraw() # do not show the tk window
    fname = filedialog.askopenfilename()

    if fname.endswith('.xml'): # Supports only XML structures at the moment
        devices = IO.getXMLcontents(fname)
    else:
        devices = IO.getDictFileContents(fname)

    wdt = simpledialog.askfloat('Widget width', 'Define {} width in px'.format(wdg), initialvalue=50, minvalue=1, maxvalue=1000)
    hgt = simpledialog.askfloat('Widget height', 'Define {} height in px'.format(wdg), initialvalue=30, minvalue=1, maxvalue=1000)
    rows = simpledialog.askinteger('Number of rows', 'Define maximum number of rows'.format(wdg), initialvalue=20, minvalue=1, maxvalue=1000)
    dest = filedialog.asksaveasfilename(filetypes=(("Phoebus files", "*.bob"),))
    IO.buildFile(wdg,devices,dest,wdt,hgt,rows)
