# PVs2PhoebusWdgs
A simple python-based (Python3) script to convert a list of PVs (.txt-file) or XML device structure to Phoebus widgets (label + widget for each PV).

## Getting started
1. Clone the package and ensure Python3 is installed.
2. Ensure the XML data structure file or the device list follows the templates
    - List: devices_list_template.txt
        - one device per row, first column being the device name and second column being the PV
    - XML: devices_xml_template.xml
        - Each device needs its name (`NAME`) and PV (`PV`) defined
3. Execute `python run.py`
4. Select widget specie, currently supported are:
    - Button
    - LED
    - Text Update
    - Check box
    - Spinner
    - Combo Box
5. Select file to load (XML data structure or list file)
6. Define the widget width
7. Define the widget height
8. Define the maximum number of rows
9. Define the name of the file
10. The file is now created and can be opened with CS Studio Phoebus
